package course.example.myandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        int numimag=Integer.parseInt(message);
        image = (ImageView) findViewById(R.id.imageView1);

        if(numimag == 1){
            image.setImageResource(R.drawable.manzana1);
        }else if(numimag==2){
            image.setImageResource(R.drawable.pina1);
        }else if(numimag==3){
            image.setImageResource(R.drawable.sandia1);
        }else{
            image.setImageResource(R.drawable.uva1);
        }
    }
}

